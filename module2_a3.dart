class Winner {
  String name;
  String category;
  String developer;
  int year;

  Winner(this.name, this.category, this.developer, this.year);

  void upperCaseAndOutput() {
    print(this.name.toUpperCase());
  }
}